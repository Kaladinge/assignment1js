const select = document.getElementById("laptops")
const loan = document.getElementById("loan")
const loanContainer = document.getElementById("loan-container")
const pay = document.getElementById("pay")
const balance = document.getElementById("balance")
const loanBtn = document.getElementById("loan-btn")
const repayBtn = document.getElementById("repay-btn")
const buyBtn = document.getElementById("buy-btn")
const workBtn = document.getElementById("work-btn")
const bankBtn = document.getElementById("bank-btn")
const features = document.getElementById("features")
const laptopImage = document.getElementById("laptop-image")
const laptopTitle = document.getElementById("laptop-title")
const laptopDescription = document.getElementById("laptop-description")
const laptopPrice = document.getElementById("laptop-price")

const url = "https://noroff-komputer-store-api.herokuapp.com/computers"
let payNumber = 0;
let bankNumber = 200;
let loanNumber = 0;
let priceNumber = 0;

pay.innerHTML = payNumber + " Kr"
balance.innerHTML = bankNumber + " Kr"


// Get laptop data
async function getData() {
    try{
        const response = await fetch(url)
        const result = await response.json()
        result.forEach(item => 
            select.innerHTML += `<option value=${item.id}>${item.title}</option>`
        )

       setLaptopData(result[0])
        
        return result;

    } catch(error) {
        console.log(error)
        laptopImage.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png"
        laptopTitle.innerHTML = "Unable to fetch data"
    }
}

const data = await getData();

function setLaptopData(data) {
    features.innerHTML = "";
    data.specs.forEach(a => features.innerHTML += `<p class="features-text">${a}</p>`)
    laptopImage.src = "https://noroff-komputer-store-api.herokuapp.com/" + data.image
    laptopTitle.innerHTML = data.title
    laptopDescription.innerHTML = data.description
    laptopPrice.innerHTML = data.price + " NOK"
    priceNumber = data.price
}

function getLoan() {
    const loanAmount = prompt("Have much do you wish to loan?");

    if (loanNumber > 0) {
        alert('Existing loan not yet paid')
    }
    else if (parseInt(loanAmount) < 2 * bankNumber) {
        loanNumber = parseInt(loanAmount);
        loanContainer.style.display = "block"
        repayBtn.style.display = "block"
        loan.innerHTML = loanNumber + " Kr"
        alert('OK desu')
    } else if (isNaN(loanAmount) || loanAmount.length == 0) {
        alert('Cannot read value')
    } else {
        alert('Loan amount is too high')
    }
}

function repayLoan() {
    loanNumber = loanNumber - payNumber
    payNumber = 0
    pay.innerHTML = payNumber + " Kr"
    loan.innerHTML = loanNumber + " Kr"
    if (Math.sign(loanNumber) === -1) {
        loanContainer.style.display = "none"
        repayBtn.style.display = "none"
        bankNumber = bankNumber - loanNumber
        loanNumber = 0
        balance.innerHTML = bankNumber + " Kr"
    }
}

function transferToBank() {
    if (loanNumber > 0) {
        bankNumber = bankNumber + (payNumber*0.9)
        loanNumber = loanNumber - (payNumber*0.1)
        if (Math.sign(loanNumber) === -1) {
            loanContainer.style.display = "none"
            repayBtn.style.display = "none"
            bankNumber = bankNumber - loanNumber
            loanNumber = 0
            balance.innerHTML = bankNumber + " Kr"
        }
    } else {
        bankNumber = bankNumber + payNumber
    }
    balance.innerHTML = bankNumber + " Kr"
    payNumber = 0
    pay.innerHTML = payNumber + " Kr"
    loan.innerHTML = loanNumber + " Kr"
}

function increaseBalance() {
    payNumber += 100
    pay.innerHTML = payNumber + " Kr"
}

function displayLaptop(e) {
    data.forEach(item => {
        if (item.id == e.target.value) {
            setLaptopData(item)
        }
    })
}

function buyLaptop() {

    if (bankNumber < priceNumber) {
        alert("There is not enough money in the bank")
    } else {
        alert("You are no the owner of a new laptop")
        bankNumber = bankNumber - priceNumber
        balance.innerHTML = bankNumber + " Kr"
    }
}

function setImage() {
    laptopImage.src = "https://via.placeholder.com/728x700.png?text=No+picture+found"
}

laptopImage.addEventListener("error", setImage)
loanBtn.addEventListener("click", getLoan)
repayBtn.addEventListener("click", repayLoan)
bankBtn.addEventListener("click", transferToBank)
workBtn.addEventListener("click", increaseBalance)
select.addEventListener("change", displayLaptop)
buyBtn.addEventListener("click", buyLaptop)