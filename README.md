# The Komputer Store

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/NicholasLennox/gradle-ci/badges/master/pipeline.svg)](https://gitlab.com/Kaladinge/assignment1js/-/commits/master)

A dynamic webpage built with JS.

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Built with](#built-with)
- [Contributing](#contributing)
- [License](#license)

## About
This project is a web application built with HTML, CSS and JavaScript. The app consists of three
areas, each with their own functionalities.

### Bank
An area in which the user can:
- see the current balance
- apply for a loan
- repay an outstanding loan

### Work
An area in which the user can:
- increase his/her earnings
- deposit cash into the bank balance

### Laptops
An area in which the user can: 
- select a laptop from a list of laptops
- display info about the chosen laptop

## Install
Clone repository

## Usage
Run application with a local live server or visit 
[Komputer Store](https://kaladinge.gitlab.io/assignment1js/) 

## Built with

- HTML
- CSS
- JavaScript

## Contributing
- [Lars-Inge Gammelsæter Jonsen](https://gitlab.com/Kaladinge)

PRs accepted.

## License

UNLICENSED
